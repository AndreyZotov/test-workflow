import React, { Component } from 'react';

class Gallery extends Component {
  render() {
    return (
      <div className="gallerypage">
        <div className="container">
          <div className="section">
            <div className="title">
              <h1>Осенняя Рапсодия</h1>
            </div>
            <div className="description">
              <p>Небольшое описание</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Gallery;
