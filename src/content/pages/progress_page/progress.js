import React, { Component } from 'react';
import TabContainer from './../../elements/tab_container/tab_container';

import './progress.css';

class Progress extends Component {
  constructor(props) {
    super(props);

    this.state = {
      addons: {
        'legion': [
          {
            title: 'Анторус, Пылающий Трон',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '9/11',
            cssClass: 'antorus',
            bosses: ['garothi', 'felhounds', 'warcouncil', 'eonar', 'portal', 'imonar', 'kingaroth', 'varimathras', 'coven', 'aggramar', 'argus']
          },
          {
            title: 'Гробница Саргераса',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '7/9',
            cssClass: 'tomb_of_sargeras',
            bosses: ['goroth', 'inquisition', 'nagabrute', 'sisters', 'sonm', 'sasszine', 'maiden', 'avatar', 'kiljaeden']
          },
          {
            title: 'Цитадель ночи',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '7/10',
            cssClass: 'nighthold',
            bosses: ['skorpyron', 'anomaly', 'trilliax', 'aluriel', 'krosus', 'tichondrius', 'botanist', 'etraeus', 'elisande', 'guldan']
          },
          {
            title: 'Испытание доблести',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '3/3',
            cssClass: 'trial_of_valor',
            bosses: ['odyn', 'guarm', 'helya']
          },
          {
            title: 'Изумрудный кошмар',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '7/7',
            cssClass: 'emerald_nightmare',
            bosses: ['nythendra', 'elerethe', 'dragons', 'ursoc', 'ilgynoth', 'cenarius', 'xavius']
          }
        ],
        'bfa': [
          {
            title: 'Рейд 1',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '9/11',
            cssClass: 'antorus',
            bosses: ['garothi', 'felhounds', 'warcouncil', 'eonar', 'portal', 'imonar', 'kingaroth', 'varimathras', 'coven', 'aggramar', 'argus']
          },
          {
            title: 'Рейд 2',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
            progress: '7/9',
            cssClass: 'tomb_of_sargeras',
            bosses: ['goroth', 'inquisition', 'nagabrute', 'sisters', 'sonm', 'sasszine', 'maiden', 'avatar', 'kiljaeden']
          }
        ]
      },
      currentAddon: 'legion'
    }

    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(event) {
    if (this.state.currentAddon !== event.target.innerHTML) {
      this.setState({currentAddon: event.target.innerHTML});
    }
  }

  renderTabContent(string) {
    return <TabContainer 
            addon={this.state.addons[string]}
            addons={this.state.addons}
            clickHandler={this.clickHandler}
            currentAddon={this.state.currentAddon}
           />
  }

  render() {
    return (
      <div className="progresspage">
        <div className="container">
          <div className="section">
            <div className="title">
              <h1>Наш прогресс</h1>
            </div>
            <div className="content">
              <section className="raid_progress__section">
                {this.renderTabContent(this.state.currentAddon)}
              </section>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Progress;
