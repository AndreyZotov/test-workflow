import React, { Component } from 'react';

import './header.css';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  clickHandler(event) {
    event.preventDefault();

    console.log(event.target);
  }

  render() {
    return (
      <div className="header">
        <nav className="container">
          <ul className="nav_menu">
            <li className="item">
              <a href='' data-name='main' onClick={this.props.clickHandler}>Главная</a>
            </li>
            <li className="item">
              <a href='' data-name='progress' onClick={this.props.clickHandler}>Прогресс</a>
            </li>
            <li className="item">
              <a href='' data-name='roster' onClick={this.props.clickHandler}>Состав</a>
            </li>
            <li className="item">
              <a href='' data-name='gallery' onClick={this.props.clickHandler}>Галерея</a>
            </li>
            <li className="item">
              <a href='' data-name='contacts' onClick={this.props.clickHandler}>Контакты</a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Header;
