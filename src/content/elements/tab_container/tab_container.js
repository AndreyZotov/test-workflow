import React, { Component } from 'react';
import Accordeon from './../../elements/accordeon/accordeon';

import './tab_container.css';

class TabContainer extends Component {
  render() {
    let tabs = [];

    for (var key in this.props.addons) {
      let className = "tab_container__nav_item"
      if (this.props.currentAddon === key) {
        className = "tab_container__nav_item active"
      }

      tabs.push(
        <div key={key} className={className} onClick={this.props.clickHandler}>
          {key}
        </div>
      );
    }

    return (
      <div className="tab_container">
        <nav className="tab_container__navigation">
          {tabs}
        </nav>
        <Accordeon addon={this.props.addon} />
      </div>
    );
  }
}

export default TabContainer;
