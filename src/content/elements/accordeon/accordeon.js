import React, { Component } from 'react';
import AccordeonItem from './accordeon_item/accordeon_item';

class Accordeon extends Component {
  constructor(props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(event) {
    event.preventDefault();
    let item = event.target.closest('.accordeon_item');
    let itemDescription = item.getElementsByClassName('description')[0];

    if (event.target.className === 'item_toggler') {
      if (item.classList.contains('expanded')) {
        item.classList.toggle('expanded');
        itemDescription.style.maxHeight = '0px';
      } else {
        if (document.documentElement.getElementsByClassName('expanded')[0]) {
          let expandedElem = document.documentElement.getElementsByClassName('expanded')[0];

          expandedElem.getElementsByClassName('description')[0].style.maxHeight = '0px';
          expandedElem.classList.toggle('expanded');
        }

        item.classList.toggle('expanded');

        let itemPaddingsHeight = parseInt(getComputedStyle(itemDescription).paddingTop, radix) + parseInt(getComputedStyle(itemDescription).paddingBottom, radix);
        itemDescription.style.maxHeight = itemDescription.scrollHeight + itemPaddingsHeight + 'px';
      }
    }
  }

  render() {
    let accordeonItems = this.props.addon.map(addon =>
      <AccordeonItem 
        key = {this.props.addon + '(' + addon.cssClass + ')'}
        title = {addon.title}
        description = {addon.description}
        progress = {addon.progress}
        cssClass = {addon.cssClass}
        bosses = {addon.bosses}
      />
    );

    return (
      <div className="accordeon" onClick={this.clickHandler}>
        {accordeonItems}
      </div>
    );
  }
}

export default Accordeon;
