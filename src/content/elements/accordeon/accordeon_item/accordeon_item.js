import React, { Component } from 'react';

import './accordeon_item.css';

class AccordeonItem extends Component {
  constructor(props) {
    super(props);

    this.renderBossIcons = this.renderBossIcons.bind(this);
  }

  renderBossIcons() {
    let urls = this.props.bosses.map((item, index) => (
      require(`./images/bosses_icons/${this.props.cssClass}/${item}.png`)
    ));
    return this.props.bosses.map((item, index) => (
      <div style={{backgroundImage: `url(${urls[index]})`}}
        className="boss_icon"
        key={`${item}-boss`}></div>
    ));
  }

  render() {
    let itemClassname = `accordeon_item ${this.props.cssClass}`;
    return (
      <div className={itemClassname}>
        <div className="title">
          <div className="raid_name">{this.props.title}</div>
          <div className="raid_progress">
            {this.props.progress}
            <span className="item_toggler">
              <i className="fas fa-angle-down"></i>
            </span>
          </div>
        </div>
        <div className="description">
          <div className="text">{this.props.description}</div>
          <div className="bosses">{this.renderBossIcons()}</div>
        </div>
      </div>
    );
  }
}

export default AccordeonItem;
