import React, { Component } from 'react';
import Header from './content/elements/header/header';
import Footer from './content/elements/footer/footer';

import Mainpage from './content/pages/main_page/main';
import Progress from './content/pages/progress_page/progress';
import Roster from './content/pages/roster_page/roster';
import Gallery from './content/pages/gallery_page/gallery';
import Contacts from './content/pages/contacts_page/contacts';

import 'normalize.css';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 'main'
    };

    this.clickHandler = this.clickHandler.bind(this);
  }

  renderBody(page) {
    let content = '';

    switch (page) {
      case 'main':
        content = <Mainpage />
        break;
      case 'progress':
        content = <Progress />
        break;
      case 'roster':
        content = <Roster />
        break;
      case 'gallery':
        content = <Gallery />
        break;
      case 'contacts':
        content = <Contacts />;
        break;
      default:
        content = <Mainpage />;
    }

    return content;
  }

  clickHandler(event) {
    event.preventDefault();
    let app = document.documentElement.getElementsByClassName('app')[0];
    this.setState({page: event.target.dataset.name});

    if (event.target.dataset.name !== 'main') {
      app.classList.add('not-main');
    } else {
      app.classList.remove('not-main');
    }
  }

  render() {
    return (
      <div className="app">
        <header>
          <Header clickHandler={this.clickHandler} />
        </header>
        <main>
          {
            this.renderBody(this.state.page)
          }
        </main>
        <footer>
          <Footer />
        </footer>
      </div>
    );
  }
}

export default App;
